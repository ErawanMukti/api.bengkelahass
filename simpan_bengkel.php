<?php
	include "koneksi.php";
	
    $id 	 = $_POST['id'];
    $nama	 = $_POST['nama'];
    $alamat  = $_POST['alamat'];
    $telepon = $_POST['telepon'];
    $email   = $_POST['email'];
    $lat 	 = $_POST['lat'];
	$lng 	 = $_POST['lng'];
	$jam 	 = isset($_POST['jam']) ? $_POST['jam'] : '';
	$gambar	 = isset($_POST['gambar']) ? $_POST['gambar'] : '';

	try{
		if ($id == '') {
			$stmt = $con->prepare('INSERT INTO bengkel (nama, alamat, no_tlp, email, jam, gambar, lat, lng)
				VALUES (:nama, :alamat, :telepon, :email, :jam, :gambar, :lat, :lng)');
			$stmt->execute(array(
				':nama' 	=> $nama,
				':alamat' 	=> $alamat,
				':telepon' 	=> $telepon,
				':email' 	=> $email,
				':jam'	 	=> $jam,
				':gambar' 	=> $gambar,
				':lat' 		=> $lat,
				':lng' 		=> $lng
			));
			$id = $con->lastInsertId();
		} else {
			$stmt = $con->prepare('UPDATE bengkel
									  SET nama = :nama,
									  	  alamat = :alamat,
									  	  no_tlp = :telepon,
									  	  email = :email,
									  	  jam = :jam,
									  	  gambar = :gambar,
									  	  lat = :lat,
									  	  lng = :lng
									WHERE id = :id');
			$stmt->execute(array(
				':id'	 	=> $id,
				':nama' 	=> $nama,
				':alamat' 	=> $alamat,
				':telepon' 	=> $telepon,
				':email' 	=> $email,
				':jam'	 	=> $jam,
				':gambar' 	=> $gambar,
				':lat' 		=> $lat,
				':lng' 		=> $lng
			));
		}		
		echo "Berhasil-$id" ;
	}catch(\PDOException $e){
		echo $e->getMessage();
	}catch(Exception $e){
		echo $e->getMessage();
	}
?>