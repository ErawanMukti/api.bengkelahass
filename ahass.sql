-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 16 Jul 2017 pada 02.18
-- Versi Server: 10.1.13-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ahass`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `bengkel`
--

CREATE TABLE `bengkel` (
  `id` int(5) NOT NULL,
  `nama` varchar(30) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `no_tlp` varchar(12) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `jam` varchar(10) DEFAULT NULL,
  `gambar` varchar(100) DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bengkel`
--

INSERT INTO `bengkel` (`id`, `nama`, `alamat`, `no_tlp`, `email`, `jam`, `gambar`, `lat`, `lng`) VALUES
(1, 'AHASS Jaya. UD', 'Jl. Ngagel Jaya Sel. No.18-AB, Pucang Sewu, Gubeng, Kota SBY, Jawa Timur (60283)', '(031) 504516', 'ahass_jaya@gmail.com', NULL, 'ahass_jaya.jpg', -7.292868, 112.753297),
(2, 'AHASS Siwalankerto Motor (1409', 'Jl. Siwalankerto No.86, Siwalankerto, Wonocolo, Kota SBY, Jawa Timur 60234', '(031) 998511', 'ahass_siwalan@gmail.com', '7:30-16:30', 'ahass_siwalan.jpg', -7.338798, 112.733309),
(3, 'AHASS Lumenindo', 'Jl. Raya Rungkut Lor No.41, Kali Rungkut, Rungkut, Kota SBY, Jawa Timur 60293', '(031) 871338', 'ahass_lumelindo@gmail.com', '8:00-16:00', 'ahass_lumelindo.jpg', -7.316503, 112.768721),
(4, 'Manna AHASS Honda Motor 02632', 'Jl. Ahmad Yani No.199, Siwalankerto, Wonocolo, Kota SBY, Jawa Timur 60234', '(031) 841576', 'ahass_manna@gmail.com', '8:00-17:00', 'ahass_manna.jpg', -7.333571, 112.73028),
(5, 'AHASS Langoan Jaya', 'Jl. Menganti Gemol No.40, Jajar Tunggal, Wiyung, Kota SBY, Jawa Timur (60223)', '(031) 709770', 'ahass_langoan@gmail.com', '7:30-16:30', 'ahass_langoan.jpg', -7.31238, 112.705847),
(6, 'Honda Imam Motor', 'Jl. Semolowaru, Semolowaru, Sukolilo, Kota SBY, Jawa Timur (60118)', '(031) 717920', 'ahass_imam_motor@gmail.com', ' 8:00-17:0', 'ahass_imam_motor.jpg', -7.300091, 112.773785),
(7, 'AHASS Panglima Jaya Motor', 'Jl. Dharmawangsa No.102, Airlangga, Gubeng, Kota SBY, Jawa Timur (60286)', '(031) 503111', 'ahass_panglima@gmail.com', '7:30-17:00', 'ahass_panglima.jpg', -7.274231, 112.756126),
(8, 'AHASS Kebonsari Motor (01463)', 'Jl. Raya Kebonsari No.42, Kebonsari, Jambangan, Kota SBY, Jawa Timur (60233)', '(031) 827058', 'ahass_kebonsari@gmail.com', '6:00-19:00', 'ahass_kebonsari.jpg', -7.330258, 112.710288),
(9, 'AHASS Utama Sari Motor 2', 'Jl. Raya Menur No.29 F, Manyar Sabrangan, Mulyorejo, Kota SBY, Jawa Timur (60286)', '(031) 594159', 'ahass_utama_sari@gmail.com', '8:00-16:00', 'ahass_utama_sari.jpg', -7.276963, 112.762475),
(10, 'AHASS 2625', 'Jl. Ngagel Jaya Utara, Pucang Sewu, Gubeng, Kota SBY, Jawa Timur (60283)', NULL, 'ahass_2625@gmail.com', NULL, 'ahass_2625.jpg', -7.291161, 112.755803),
(11, 'AHASS 8332 MITRA KARYA', 'Jl. Gayung Kb. Sari, Kebonsari, Jambangan, Kota SBY, Jawa Timur (60235)', NULL, 'ahass_mitra@gmail.com', NULL, 'ahass_mitra.jpg', -7.32846, 112.718734),
(12, 'AHASS PRATAMA 01794', 'Jl. Raya Margorejo Indah No. 21, Wonocolo, Jemur Wonosari, Surabaya, Jawa Timur (60237)', '(031) 847065', 'ahass_pratama@gmail.com', '7:30-16:30', 'ahass_pratama.jpg', -7.316791, 112.736702),
(13, 'Angwin Motor - AHASS Honda 139', 'Jl. Ngagel Jaya Sel. No.66, Ngagelrejo, Wonokromo, Kota SBY, Jawa Timur (60245)', '(031) 502127', 'ahass_angwin@gmail.com', '8:00-16:30', 'ahass_angwin.jpg', -7.293513, 112.754946),
(14, 'AHASS Untung Jaya', 'Jl. Banyu Urip Wetan V No.95, Putat Jaya, Kec. Sawahan, Kota SBY, Jawa Timur (60255)', '(031) 568450', 'ahass_untung@gmail.com', NULL, 'ahass_untung.jpg', -7.255982, 112.683907),
(15, 'ABC Moto Honda AHASS', 'Jl. Bendul Merisi No.18, Jagir, Wonokromo, Kota SBY, Jawa Timur (60244)', NULL, 'abc_moto@gmail.com', '7:30-16:30', 'abc_moto.jpg', -7.305853, 112.739604),
(16, 'AHASS Panji Perkasa Perdana', 'Jl. Manyar Kertoarjo No.48, Mojo, Gubeng, Kota SBY, Jawa Timur (60116)', '(031) 592584', 'ahass_panji@gmail.com', '7:30-16:30', 'ahass_panji.jpg', -7.279718, 112.767198),
(17, 'AHASS Eka Jaya Motor', 'Jl. Ketintang Barat. No.48, Ketintang, Gayungan, Kota SBY, Jawa Timur (60231)', '(031) 827590', 'ahass_eka_jaya@gmail.com', '8:00-16:00', 'ahass_eka_jaya.jpg', -7.308298, 112.722777),
(18, 'AHASS Indo Motor 16', 'Jl. Bratang Gede No.16, Ngagelrejo, Wonokromo, Kota SBY, Jawa Timur (60245)', '(031) 501034', 'ahass_indo_motor@gmail.com', '8:00-16:00', 'ahass_indo_motor.jpg', -7.297811, 112.752279);

-- --------------------------------------------------------

--
-- Struktur dari tabel `review`
--

CREATE TABLE `review` (
  `id_review` int(11) NOT NULL,
  `id_bengkel` int(11) NOT NULL,
  `tgl_review` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `nama_user` varchar(50) NOT NULL,
  `email_user` varchar(50) NOT NULL,
  `isi_review` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `username`, `password`) VALUES
(1, 'admin', 'admin-123');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bengkel`
--
ALTER TABLE `bengkel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`id_review`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bengkel`
--
ALTER TABLE `bengkel`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `review`
--
ALTER TABLE `review`
  MODIFY `id_review` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
