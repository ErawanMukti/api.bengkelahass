<?php
	include "koneksi.php";
	
    $bengkel = $_POST['id_bengkel'];
    $nama	 = $_POST['nama'];
    $email   = $_POST['email'];
    $isi 	 = $_POST['isi'];

	try{
		$stmt = $con->prepare('INSERT INTO review (id_bengkel, nama_user, email_user, isi_review)
			VALUES (:id, :nama, :email, :isi)');
		$stmt->execute(array(
			':id'	 	=> $bengkel,
			':nama' 	=> $nama,
			':email' 	=> $email,
			':isi'	 	=> $isi
		));

		echo "Berhasil" ;
	}catch(\PDOException $e){
		echo $e->getMessage();
	}catch(Exception $e){
		echo $e->getMessage();
	}
?>