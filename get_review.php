<?php
	include "koneksi.php";

	$id 	= isset($_POST['id']) ? $_POST['id'] : '%';
	$hasil 	= array();

	try{
		$stmt = $con->prepare("SELECT * FROM review WHERE id_bengkel = :id ORDER BY id_review DESC");
		$stmt->execute(array(':id' => $id));

		$isi = $stmt->fetchAll();
		foreach($isi as $s){
			array_push($hasil, array(
				'tgl' 	=> $s['tgl_review'],
				'nama'  => $s['nama_user'],
				'email' => $s['email_user'],
				'isi' 	=> $s['isi_review']
			));
		}

		echo json_encode(array("hasil"=>$hasil));
	}catch(\PDOException $e){
		echo $e->getMessage();
	}catch(Exception $e){
		echo $e->getMessage();
	}	
?>