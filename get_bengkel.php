<?php
	include "koneksi.php";

	//$id = isset($_POST['id']) ? $_POST['id'] : '%';
	$hasil = array();

	try{
		$stmt = $con->prepare("SELECT * FROM bengkel");
		$stmt->execute();

		$isi = $stmt->fetchAll();
		foreach($isi as $s){
			array_push($hasil, array(
				'id'     => $s['id'],
				'nama' 	 => $s['nama'],
				'alamat' => $s['alamat'],
				'tlp' 	 => $s['no_tlp'],
				'email'	 => $s['email'],
				'jam' 	 => $s['jam'],
				'gambar' => $s['gambar'],
				'lat' 	 => $s['lat'],
				'lng' 	 => $s['lng']
			));
		}

		echo json_encode(array("hasil"=>$hasil));
	}catch(\PDOException $e){
		echo $e->getMessage();
	}catch(Exception $e){
		echo $e->getMessage();
	}	
?>